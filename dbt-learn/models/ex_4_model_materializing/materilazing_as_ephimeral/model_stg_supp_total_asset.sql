{{config (
    database ='learn_dbt',
    schema ='stage',
    materialized='view'
)}}


with part_supp as (
    select * from {{ref('model_stg_partsupp')}}
    )
    
select 
    *,
    (PS_AVAILQTY * PS_SUPPLYCOST) as supplier_total_asset_cost 
from part_supp
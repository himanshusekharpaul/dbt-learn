-- This model will create a transient table  with name MODEL_STG_REGION_TRN_TBL 
-- in schema STAGE of  database LEARN_DBT

-- To Run it : dbt run -m model_stg_region_trn_tbl


{{config (
    database ='learn_dbt',
    schema ='stage',
    materialized='table'
    )}}

SELECT * FROM learn_dbt.stage.region


-- This model will create a Secure view with name MODEL_STG_NATION_SECURE
-- in schema STAGE of  database LEARN_DBT

-- To Run it : dbt run -m model_stg_nation_secure

{{config (
    database ='learn_dbt',
    schema ='stage',
    materialized="view",
    secure=true
)}}

SELECT * FROM learn_dbt.stage.nation
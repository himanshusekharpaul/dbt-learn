-- This model will create a standard view with name MODEL_STG_NATION 
-- in schema STAGE of  database LEARN_DBT

-- To Run it : dbt run -m model_stg_nation


{{config (
    database ='learn_dbt',
    schema ='stage',
    materialized="view"
)}}

SELECT * FROM learn_dbt.stage.nation
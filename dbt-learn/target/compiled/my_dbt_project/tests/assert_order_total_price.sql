-- Agenda        : Test If there is any total order price is less than 0
-- Test Criteria : If number of records returned from query is 0 
--                              then test is 'Passed' else 'Failed' 

-- To Run this test :  dbt test --data   or dbt test -m stg_order



select O_TOTALPRICE, count(*)
  from learn_dbt.stage.model_stg_order
 where O_TOTALPRICE <0
 group by O_TOTALPRICE
-- Agenda        : Test If there is any order status other than 'F', 'O',  'P'
-- Test Criteria : If number of records returned from query is 0 
--                              then test is 'Passed' else 'Failed' 
-- To Run this test :  dbt test --data   or dbt test -m stg_order




select O_ORDERSTATUS, count(*)
  from learn_dbt.stage.model_stg_order
 where O_ORDERSTATUS not in ('F','O','P')
 group by O_ORDERSTATUS
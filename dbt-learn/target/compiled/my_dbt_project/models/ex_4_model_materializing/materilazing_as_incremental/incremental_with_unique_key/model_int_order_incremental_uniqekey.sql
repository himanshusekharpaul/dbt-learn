-- Agenda : Perform incremnatal load 
--          from  Stage Schema to Integration Schema 
--          based on unique key  based Incremental column 

-- ***********************************************
-- To Compile The File               :   dbt compile -m model_int_order_incremental_unique_key
-- To run for full load (first time) :   dbt run --full-refresh -m model_int_order_incremental_unique_key
-- To run for incremental            :   dbt run -m model_int_order_incremental_unique_key

-- To Validate : SELECT   O_REC_INSERT_TS,COUNT(*)
--                 FROM   DBT_EXERCISE.INT.IN_ORDER_INCREMENTAL_TS
--                GROUP   BY O_REC_INSERT_UNIQUE_KEY




select * from learn_dbt.stage.model_stg_order


  where o_seq >= (select max(o_seq) from learn_dbt.stage.model_int_order_incremental_uniqekey)




with  __dbt__cte__model_stg_partsupp as (


select *
from snowflake_sample_data.tpch_sf10.partsupp
),part_supp as (
    select * from __dbt__cte__model_stg_partsupp
    )
    
select 
    *,
    (PS_AVAILQTY * PS_SUPPLYCOST) as supplier_total_asset_cost 
from part_supp
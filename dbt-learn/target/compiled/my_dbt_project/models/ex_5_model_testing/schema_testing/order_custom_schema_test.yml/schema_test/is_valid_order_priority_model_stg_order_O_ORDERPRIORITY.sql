

with priority_validation as (
    select O_ORDERPRIORITY as order_priority
    from   learn_dbt.stage.model_stg_order
	),

validation_errors as (
    select  order_priority 
	from priority_validation
    where order_priority not in ('1-URGENT','2-HIGH','3-MEDIUM','4-NOT SPECIFIED','5-LOW')
	)

select *
from validation_errors


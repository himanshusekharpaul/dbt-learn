
    
    

with child as (
    select C_NATIONKEY as from_field
    from learn_dbt.stage.model_src_customer
    where C_NATIONKEY is not null
),

parent as (
    select N_NATIONKEY as to_field
    from learn_dbt.stage.model_stg_nation
)

select
    from_field

from child
left join parent
    on child.from_field = parent.to_field

where parent.to_field is null



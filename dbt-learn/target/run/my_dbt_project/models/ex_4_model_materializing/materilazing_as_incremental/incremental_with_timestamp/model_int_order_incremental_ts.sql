begin;
    

        insert into learn_dbt.stage.model_int_order_incremental_ts ("O_SEQ", "O_ORDERKEY", "O_CUSTKEY", "O_ORDERSTATUS", "O_TOTALPRICE", "O_ORDERDATE", "O_ORDERPRIORITY", "O_CLERK", "O_SHIPPRIORITY", "O_COMMENT", "O_REC_INSERT_TS")
        (
            select "O_SEQ", "O_ORDERKEY", "O_CUSTKEY", "O_ORDERSTATUS", "O_TOTALPRICE", "O_ORDERDATE", "O_ORDERPRIORITY", "O_CLERK", "O_SHIPPRIORITY", "O_COMMENT", "O_REC_INSERT_TS"
            from learn_dbt.stage.model_int_order_incremental_ts__dbt_tmp
        );
    commit;
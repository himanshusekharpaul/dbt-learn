select
      count(*) as failures,
      count(*) != 0 as should_warn,
      count(*) != 0 as should_error
from (

        with all_values as (
                select
                        C_MKTSEGMENT as value_field,
                        count(*) as n_records
                from learn_dbt.stage.model_src_customer
                group by C_MKTSEGMENT
                )
        select *
        from all_values
        where value_field not in ( 'MACHINERY','HOUSEHOLD','FURNITURE','AUTOMOBILE','BUILDING')

    ) dbt_internal_test

    
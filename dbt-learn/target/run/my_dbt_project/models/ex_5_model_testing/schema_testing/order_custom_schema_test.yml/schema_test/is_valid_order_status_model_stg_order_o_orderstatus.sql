select
      count(*) as failures,
      count(*) != 0 as should_warn,
      count(*) != 0 as should_error
from  (
            with status_validation as (
                    select o_orderstatus as order_status
                    from   learn_dbt.stage.model_stg_order
	                ),

                validation_errors as (
                    select  order_status 
	                from status_validation
                    where order_status not in ('F','O','P')
	                )

            select *
            from validation_errors
      ) dbt_internal_test
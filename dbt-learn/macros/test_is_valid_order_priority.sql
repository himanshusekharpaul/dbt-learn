{% test is_valid_order_priority(model, column_name) %}

with priority_validation as (
        select {{ column_name }} as order_priority
        from   {{ model }}
	),

    validation_errors as (
        select  order_priority 
	    from priority_validation
        where order_priority not in ('1-URGENT','2-HIGH','3-MEDIUM','4-NOT SPECIFIED','5-LOW')
	)

select *
from validation_errors

{% endtest %}


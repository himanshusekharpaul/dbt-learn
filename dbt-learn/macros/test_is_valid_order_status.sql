{% test is_valid_order_status(model, column_name) %}

with status_validation as (
        select {{ column_name }} as order_status
        from   {{ model }}
	),

validation_errors as (
        select  order_status 
	    from status_validation
        where order_status not in ('F','O','P')
	)

select *
from validation_errors

{% endtest %}



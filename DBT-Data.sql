CREATE DATABASE learn_dbt;

--Create Schema
CREATE OR REPLACE SCHEMA learn_dbt.stage;
CREATE OR REPLACE SCHEMA learn_dbt.integration;
CREATE OR REPLACE SCHEMA learn_dbt.mart;

-- Preparing Sample Data

CREATE OR REPLACE SEQUENCE  learn_dbt.public.odr_seq;


CREATE OR REPLACE TABLE learn_dbt.public.orders_raw (
    O_SEQ NUMBER(38,0) DEFAULT odr_seq.nextval,
	O_ORDERKEY NUMBER(38,0) NOT NULL,
	O_CUSTKEY NUMBER(38,0) NOT NULL,
	O_ORDERSTATUS VARCHAR(1) NOT NULL,
	O_TOTALPRICE NUMBER(12,2) NOT NULL,
	O_ORDERDATE DATE NOT NULL,
	O_ORDERPRIORITY VARCHAR(15) NOT NULL,
	O_CLERK VARCHAR(15) NOT NULL,
	O_SHIPPRIORITY NUMBER(38,0) NOT NULL,
	O_COMMENT VARCHAR(79) NOT NULL
);


INSERT INTO learn_dbt.public.orders_raw (o_orderkey,o_custkey,o_orderstatus,o_totalprice,o_orderdate,o_orderpriority,o_clerk,o_shippriority,o_comment)
SELECT O_ORDERKEY,O_CUSTKEY,O_ORDERSTATUS,O_TOTALPRICE,O_ORDERDATE,O_ORDERPRIORITY,O_CLERK,O_SHIPPRIORITY,O_COMMENT
FROM snowflake_sample_data.tpch_sf10.orders 
ORDER BY O_ORDERKEY;


CREATE OR REPLACE TABLE learn_dbt.stage.orders (
    O_SEQ NUMBER(38,0) ,
	O_ORDERKEY NUMBER(38,0) NOT NULL,
	O_CUSTKEY NUMBER(38,0) NOT NULL,
	O_ORDERSTATUS VARCHAR(1) NOT NULL,
	O_TOTALPRICE NUMBER(12,2) NOT NULL,
	O_ORDERDATE DATE NOT NULL,
	O_ORDERPRIORITY VARCHAR(15) NOT NULL,
	O_CLERK VARCHAR(15) NOT NULL,
	O_SHIPPRIORITY NUMBER(38,0) NOT NULL,
	O_COMMENT VARCHAR(79) NOT NULL,
    O_REC_INSERT_TS TIMESTAMP
);


--Batch 1 Data Load
INSERT INTO learn_dbt.stage.orders
SELECT *,CURRENT_TIMESTAMP from dbt_exercise.stg.orders_raw 
WHERE O_SEQ <100000
ORDER BY O_SEQ;






CREATE OR REPLACE TABLE learn_dbt.stage.customer 
LIKE snowflake_sample_data.tpch_sf10.customer;


INSERT INTO learn_dbt.stage.customer
SELECT * 
FROM snowflake_sample_data.tpch_sf10.customer;



CREATE OR REPLACE TABLE learn_dbt.stage.nation
AS SELECT * FROM snowflake_sample_data.tpch_sf10.nation;

CREATE OR REPLACE TABLE learn_dbt.stage.region
AS  SELECT * FROM  snowflake_sample_data.tpch_sf10.region;